# First django app
This is my first django app, it follows the tutorial on https://docs.djangoproject.com/en/1.11/intro/tutorial01/

# Setup
In order to work on this project you should prepare a virtual environment in this folder
```bash
virtualenv --python=python3.6 venv-3.6
```
and switch to it
```bash
source venv-3.6/bin/activate
```

# Compile django-polls
The polls application is a module that needs to be installed as library first, before we can do that we need to build it.
In your virtual environment go into the folder django-polls and execute
```bash
python setup.py sdist
```
after building you can install the built library in with pip like so:
```bash
pip install django-polls/dist/django-polls-0.1.tar.gz
```

# Setup django and the admin UI
Now that we have the library setup, we can focus on creating the django app and the admin ui.
First we need to install django, so execute the following command in your viratual environment
```bash
pip install django
```
After that we need to create the database, as the app has some migration scripts we need to execute them with the following command in the mysite folder:
```bash
python manage.py migrate
```
Now we are ready to launch the dev server, however in order to use the admin ui we need to create a superadmin user first by executing:
```bash
python manage.py createsuperuser
```

# Run the development server
To run the development server, just execute the following command in your mysite folder
```bash
python manage.py runserver
```
By default the server runs on http://127.0.0.1:8000/ you can change this behavior, please consult the django documentation for that.
Our application is deployed on http://127.0.0.1:8000/polls however there are no polls created yet, so you need to login as admin on http://127.0.0.1:8000/admin with your created superuser account to create your first polls.
